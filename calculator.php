<!DOCTYPE html>
<head>
    <title> PHP Calculator </title>
</head>
<body>
    <h1> Welcome to this simple calculator! </h1>
    <br />
    <form action="http://ec2-54-213-253-170.us-west-2.compute.amazonaws.com/~drewkaplan/calculatorresults.php" method="GET">
        <p>
            <label for="firstnuminput">First Number:</label>
            <input type="number" step="any" name="firstnum" id="firstnuminput"/>
        </p>
        <p>
            <input type="radio" name="function" value="add" id='addinput'/> <label for="addinput"> + </label>
            <input type="radio" name="function" value="sub" id="subinput"/><label for="subinput"> - </label>
            <input type="radio" name="function" value="mult" id="multinput"/><label for="multinput"> x </label>
            <input type="radio" name="function" value="div" id="divinput"/><label for="divinput"> / </label>
        </p>
        <p>
            <label for="secondnuminput">Second Number:</label>
            <input type="number" step="any" name="secondnum" id="secondnuminput"/>
        </p>
            <input type="submit" value="Calculate">
    </form>
</body>
</html>