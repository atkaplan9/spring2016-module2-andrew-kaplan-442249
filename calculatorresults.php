<!DOCTYPE html>
<head>
<title> PHP Calculator Results</title>
</head>
<body>
    <h1> Here are your results! </h1>
<?php
$firstnum = $_GET['firstnum'];
$secondnum = $_GET['secondnum'];
$function = $_GET['function'];
$skipprint = false;

if ($function == "add"){
    $action = " + ";
    $result = $firstnum + $secondnum;
}
elseif($function == "sub"){
    $action = " - ";
    $result = $firstnum - $secondnum;
}
elseif($function == "mult"){
    $action = " x ";
    $result = $firstnum * $secondnum;
}
elseif($function == "div"){
    $action = " / ";
    if ($secondnum == 0){
        echo "You can not divide by zero. Please input a new second number.";
        $skipprint = true;
        
    }
    else{
    $result = $firstnum / $secondnum;
    }
}

if($skipprint == false){
printf("\t<p>The result of %d %s %d is %.2f</p>",
       htmlentities($firstnum),
       htmlentities($action),
       htmlentities($secondnum),
       htmlentities($result)
       );
}
?>
    
</body>
</html>